FROM node:12.20.1-alpine3.12

ENV PEFIX_WEB_APP =''
ENV AUTH_WEB_APP =''
ENV POSTMAN_API_KEY =''
ENV WEBHOOK_URL_PATH=''
ENV POSTMAN_API_URL='https://api.getpostman.com/'
ENV STATIC_REPORT_URL=''
ENV WEB_PORT_APP=''

RUN apk --no-cache add procps=3.3.16-r0 curl jq=1.6-r1 bash=5.0.17-r0 && npm install pm2@^4.4.1 -g && mkdir /etc/nodeserver

WORKDIR /etc/nodeserver

COPY  cfg.json /etc/nodeserver/cfg.json
COPY  server.js /etc/nodeserver/server.js
COPY  middlewares.js /etc/nodeserver/middlewares.js
COPY  package.json /etc/nodeserver/package.json
COPY  package-lock.json /etc/nodeserver/package-lock.json
COPY  ecosystem.config.js /etc/nodeserver/ecosystem.config.js

COPY  htmlreport/ /etc/nodeserver/htmlreport/
COPY  public/ /etc/nodeserver/public/
COPY  middlewares/ /etc/nodeserver/middlewares/
COPY  network/ /etc/nodeserver/network/
COPY  components/ /etc/nodeserver/components/
COPY  controllers/ /etc/nodeserver/controllers/
COPY  crt/ /etc/nodeserver/crt/
COPY  testfile/ /etc/nodeserver/testfile/


COPY  features/ /etc/nodeserver/features/
COPY  scripts/ /etc/nodeserver/scripts/
COPY  data/  /etc/nodeserver/data/
COPY  batchFiles/ /etc/nodeserver/batchFiles/
COPY  environment/ /etc/nodeserver/environment/
COPY  Web_hook_tools/ /etc/nodeserver/Web_hook_tools/

RUN npm ci

WORKDIR /etc/nodeserver
EXPOSE 3001

CMD ["pm2-runtime", "start", "ecosystem.config.js"]