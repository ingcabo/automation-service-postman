const express = require('express');
const middlewares = require('./middlewares');
var https = require('https');
var http = require('http');
const router = require('./network/routes');
var fs = require('fs');
const HttpsPort = 443;
const HttpPort = process.env.WEB_PORT_APP;
const httpsOptions ={
    key: fs.readFileSync('./crt/serverQaService.key', 'utf8'),
    cert: fs.readFileSync('./crt/clientQaService.cert', 'utf8')
}

var app = express();

middlewares(app);  
router(app);

app.options('/*',function(req,res){
    req.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    req.header("Access-Control-Allow-Methods", "*");
    req.header("Access-Control-Allow-Headers", "*");
});

//static Server
app.use('/app',express.static('public'));

/*
https.createServer(httpsOptions ,app).listen(HttpsPort, function(){
    console.log(`se esta escuchando SSL en https://localhost:${HttpsPort}`);
});
*/

http.createServer(httpsOptions ,app).listen(HttpPort, function(){
    console.log(`ON ... http://localhost:${HttpPort}`);
});
