const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  defaultMeta: { service: 'automation-services' },
  format: winston.format.combine(
    winston.format.label({ label: 'right meow!' }),
    winston.format.timestamp(),
    winston.format.printf(
      info => info.timestamp + ' ' + info.level.toUpperCase() + ': ' + info.message
      )
  ),
  transports: [
    new winston.transports.Console()
  ],
});

module.exports = logger
