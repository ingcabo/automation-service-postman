const fs  = require('fs');
const cfg = require('../cfg.json');


function readFilesFolder(folder=cfg.dirReport, enconding='utf-8') {
    
    return new Promise(function(resolve, reject) {
        fs.readdir(folder,enconding, function(err, filenames){
            if (err) 
                reject(err); 
            else 
                
                resolve(filenames);
        });
    });
};

//if we need to get save the data/env/collections files tests - change by aws s3 method
async function moveFile (file,patch){
    file.mv(patch + file.name);
    return patch+file.name;
}


//if we need to get read the configuration files of the tests - change by aws s3 method
function readFile(file) {
    return fs.readFileSync(file, { encoding: 'utf8', flag: 'r' });
}


//if we need to write the configuration files of the tests - change by aws s3 method
function writeFile(InputValue,file) {
    if (typeof(InputValue) === 'object'){
        InputValue = JSON.stringify(InputValue);
    }
    return fs.writeFile(file, InputValue, 'utf8', function (err) {
        if (err) throw err;
    });
}


function toDay(days = 0,format=10, char='T'){
    let date = new Date();
    date.setDate(date.getDate() + days);
    date = date.toISOString().slice(0, format).replace(char, '');
    return date;
  }

  function formatDate (){
    const date = new Date();
    const timestamp = date.getTime();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const formattedDate = `-${day}-${month}-${year}-${timestamp}`;
    return formattedDate;
  };

module.exports = {
    readFilesFolder,
    moveFile,
    readFile,
    writeFile,
    toDay,
    formatDate
}





