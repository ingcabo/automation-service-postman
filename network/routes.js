
const test = require('../components/test_service/network');
const filesfortest  = require('../components/filesForTest/network');
const getexecute = require('../components/getAutomation/network');
const jokes = require('../components/jokes/network');
const settest = require('../components/fileSetTest/netwoork');
const postmanApi = require('../components/postman-api/network');
const batchValidator = require('../components/batch-processing-validation/network.js'); 


const routes = function(server){
    server.use(`/jokes`,jokes);
    server.use(`/test`,test);
    server.use(`/filesfortest`,filesfortest);
    server.use(`/getexecute`,getexecute);
    server.use(`/settest`,settest);
    server.use(`/postman-api`,postmanApi);  
    server.use(`/batch-validator`,batchValidator);    
}

module.exports = routes;