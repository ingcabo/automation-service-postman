const axios = require('axios');
const https = require('https');
const {URL_WEB_APP,AUTH_WEB_APP} = require('../components/nx_user/environment/userType_stage');


const axiosClient = axios.create({
    baseURL: URL_WEB_APP,
    headers: {
        'Content-Type': 'application/json',
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});


module.exports = {
    axiosClient
}


function unix_time  (timer) {
    Date.prototype.toUnixTime = function() { return this.getTime()/1000|0 };
    Date.time = function() { return new Date().toUnixTime(); }
    let time = Date.time();
    console.log(time);
    return time;
  }



  function showOffset() {
  
    // Date object
    var date = new Date('August 21, 2000 18:02:25 GMT+05:00');
    // Offset variable will store 
    // timezone offset between 
    // UTC and your local time   
    var offset = date.getTimezoneOffset();
    return offset;


} 
console.log(showOffset())