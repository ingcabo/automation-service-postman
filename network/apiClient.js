const {getClient} = require('./getClient.js');

class ApiClient {
    constructor(baseUrl = null,userToken="", tokenAdmin="") {
      this.client = getClient(baseUrl,userToken,tokenAdmin);
    }
    
    get(url, conf = {}) {
      return this.client.get(url, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    delete(url, conf = {}) {
      return this.client.delete(url, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    head(url, conf = {}) {
      return this.client.head(url, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    options(url, conf = {}) {
      return this.client.options(url, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    post(url, data = {}, conf = {}) {
      return this.client.post(url, data, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    put(url, data = {}, conf = {}) {
      return this.client.put(url, data, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
    patch(url, data = {}, conf = {}) {
      return this.client.patch(url, data, conf)
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error));
    }
  }

  module.exports ={
    ApiClient,
}

