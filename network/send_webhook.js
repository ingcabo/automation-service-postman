const { IncomingWebhook } = require('@slack/webhook');
const {ApiClient } = require('./apiClient.js')
const WEBHOOK_URL_PATH = process.env.WEBHOOK_URL_PATH;
const MT_WEBHOOK_URL_PATH = process.env.MT_WEBHOOK_URL_PATH;

function slack_send_msg(message,sendTo ='bg7AqkZfVvJdOxH4dgS69Q54'){
  
    var completeUrl =`${WEBHOOK_URL_PATH}/${sendTo}`;
    console.log('URL======$$$$$$',completeUrl)
    const webhookf = new IncomingWebhook(completeUrl);
    var error='#FF0000';
    var success='#1AC107';
    var mycolor = success;
    var exist = message.indexOf('Failed Test'); 

    if (exist !== -1 ) {
        mycolor = error;
    }

    return new Promise( async (resolve,reject) =>{
       
        const result = await webhookf.send({
            type: "mrkdwn",
            text: "",
            attachments:[
                {
                text:`${message}`,
                color:mycolor,
                footer:"company",
                footer_icon:"https://images.ctfassets.net/drib7o8rcbyf/6w2Y64NVtYvHGBs8vF9a3W/64360873a9e45c623222355b69011fa0/equinox-logo.png"
            }
        ]
          });

        resolve(result);
    })
}

async function msTeams_send_msg(body,sendto="fdf0768e20dd427593c41e95f808ac9b/d135f778-4931-4339-8938-ec4eaf8f7140"){

    let client = new ApiClient(MT_WEBHOOK_URL_PATH,"","");
    var error='#FF0000';
    var success='#1AC107';
    var mycolor = success;
    //var exist = text.indexOf('Failed Test'); 

    let color=mycolor;
    let footer="Fallabela";
    let footer_icon="./logo.png";

    return await client.post(sendto,body,null);
}


module.exports = {
    slack_send_msg,
    msTeams_send_msg
}


