const axios = require('axios');
const https = require('https');
const Raven = require('raven-js');
const AxiosLogger = require('axios-logger');

function getClient  (baseUrl = null, userToken="", tokenAdmin="")  {

 const options = {
    baseURL: baseUrl,
    headers: {
        'Content-Type': 'application/json',
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
  };

 if (tokenAdmin.length > 12) {
    options.headers.patch = {
      ...options.headers,
      Authorization: `Bearer ${tokenAdmin}`,
    };
  }

  if (userToken.length > 12) {
    options.headers.post = {
      ...options.headers,
      Authorization: `Bearer ${userToken}`,
    };
  }




const client = axios.create(options);

  // Add a request interceptor
  client.interceptors.request.use(
    requestConfig => requestConfig,
    (requestError) => {
      Raven.captureException(requestError);
      return Promise.reject(requestError);
    },
  );

      // Add a response interceptor
      client.interceptors.response.use(
        response => response,
        (error) => {
          if (error.response.status >= 500) {
            Raven.captureException(error);
          }
          return Promise.reject(error);
        },
      );
  
    

  return client;
  
};

module.exports = {getClient};

