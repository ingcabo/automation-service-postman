const response = require('../network/response');
const logger = require('../network/logger')

const basic_auth_middleware = function(req, res, next) {
    
    
    if (req.headers.authorization && req.headers.authorization.search('Basic ') === 0) {
        let AUTH_WEB_APP =process.env.AUTH_WEB_APP.toString();
        const pass =req.headers.authorization.toString();
        if (pass == AUTH_WEB_APP) {
            next();
            return;
        }
    }
    logger.info('Unable to authenticate user');
    res.header('WWW-Authenticate', 'Basic realm="Admin Area"');
    if (req.headers.authorization) {
        setTimeout(function () {
            response.error(req, res,'Authentication required.', 401, 'Authentication required.');
            
        }, 5000);
    } else {
        response.error(req, res, 'Authentication required..', 401, 'Authentication required..');
        
    }
}

const decode64 =(value) =>{
    let str = value.substring(6, value.length);
    let buff = new Buffer.from(str, 'base64');
    str = buff.toString('ascii');
    return str;
}



module.exports = basic_auth_middleware;