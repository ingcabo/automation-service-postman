const logger = require('../network/logger');

module.exports = function (req, res, next) {
  res.on('finish', function(){
      logger.info(req.method.toUpperCase() + ' ( ' + res.statusCode + ' ) ' + req.originalUrl )
  })
  next()
}