const newman = require('newman');
const logger = require('../network/logger.js')
const postmanApi = require('../components/postman-api/controller')
//const user_pool = require('../network/toolbox.js');
const path = require('path')
const fs = require('fs');
const {dirReport,dirData,dirEnv,dirFeat} =require('../cfg.json');

const DATA_PATH = path.resolve(dirData);
const ENVIRONMENT_PATH = path.resolve(dirEnv);
const FEATURE_PATH = path.resolve(dirFeat);

const start = (error, args) => {
  try {
    logger.info('running a collection...');
    if (error) {
      logger.error(error);
      logger.error(error.stack);
    }
  } catch(error) {
    logger.error(error);
  }
}

const beforeDone = (error, args) => {
  try {
    logger.info("beforeDone");
    if (error) {
      logger.error(error);
      logger.error(error.stack);
    } 
  } catch(error) {
    logger.error(error);
  }
}

const beforeRequest = (error, args) => {
  try {
    logger.info("beforeRequest");
    if (error) {
        logger.error(error);
        logger.error(error.stack);
    }
  } catch(error) {
    logger.error(error);
  }
}

const request = (error, args) => {
  try {
    logger.info("request");
    if (error) {
        logger.error(error);
        logger.error(error.stack);
    }
  } catch(error) {
    logger.error(error);
  }
}

const done = (error, summary) => {
  try {
    logger.info("done");
    
    if (error) {
        logger.error(error);
        logger.error(error.stack);
    } else {
      logger.info("finish runner...");
      resolve(summary.run);
    }
  } catch(error) {
    logger.error(error);
  }
}

const callback = (err, summary) => {
  if (err) {
    reject(err);
  }
}

const collectionParam = (collectionName, collection) => {
  return new Promise((resolve, reject) => {
    if (collectionName) {
      postmanApi.collections({name: collectionName}).then(collections => {
        resolve(postmanApi.collectionUrl(collections.data[0].uid));
      }).catch(err => {
        reject(err)
      })
    } else {
      resolve(path.join(FEATURE_PATH, collection))
    }
  })
}

const environmentParam = (environmentName, environment) => {
  return new Promise((resolve, reject) => {
    if (environmentName) {
      postmanApi.environments({name: environmentName}).then(environments => {
        resolve(postmanApi.environmentUrl(environments.data[0].uid));
      }).catch(err => {
        reject(err)
      })
    } else if ((environment != undefined) && (environment.length > 0 )) {
      resolve(path.join(ENVIRONMENT_PATH, environment));
    } else {
      resolve(null);
    }
  })
}

const dataParam = (iterationData) => {
  return (iterationData) ? path.join(DATA_PATH, iterationData) : null
}

function runcollection(profile){
    let profileName = profile.name;
    let profileCollection = profile.collection;
    let profileCollectionName = profile.collectionName;
    let profileEnvironment = profile.environment;
    let profileEnvironmentName = profile.environmentName;
    //let profileEnvironmentName = profile.environmentname || process.env.ENV_NAME;
    let profileBail = profile.bail;
    let profileIterationData = profile.iterationData;
    let profileIterationCount = profile.iterationCount;
    let ReportTimingName = profile.timeForResportTestNameHtml;
    let collectionPathPromise = collectionParam(profileCollectionName, profileCollection)
    let environmentPathPromise = environmentParam(profileEnvironmentName, profileEnvironment)

    //address where I save the report
    let UrlTestReport = `${dirReport}/${profileName}-${ReportTimingName}.html`;


    return Promise.all([collectionPathPromise,environmentPathPromise]).then((results) => {
      let isBail = (profileBail == 'true') ? [`${dirReport}`, 'failure'] : "";
      let newmanParams = {
        collection: results[0],
        iterationData:  dataParam(profileIterationData),
        environment: results[1],
        bail: isBail,
        insecureFileRead:true,
        iterationCount:profileIterationCount || '',
        reporters: [
          'htmlextra',
          'cli'
        ],
        reporter : {
          htmlextra: {
            export: UrlTestReport,
            logs: true,
            showEnvironmentData: true,
            showMarkdownLinks: true
          }
        }
      }
      logger.info(JSON.stringify(newmanParams))
      return new Promise((resolve, reject) => {
        newman.run(newmanParams, (err, summary) => {
          if (err) {
            reject(err);
          }
        }).on('start', start)
          .on('beforeDone', beforeDone)
          .on('beforeRequest', beforeRequest)
          .on('request', request)
          .on('done', (error, summary) => {
            try {
              logger.info("done");    
              if (error) {
                  logger.error(error);
                  logger.error(error.stack);
              } else {
                logger.info("finish runner...");
                //resolve(summary.run);
                resolve(summary);
                console.log('summary  ...',summary);
              }
            } catch(error) {
              logger.error(error);
            }
          })
      })
  })
}


 
module.exports = {
    runcollection,
}

//https://github.com/postmanlabs/newman
//https://www.npmjs.com/package/newman#api-reference
//https://ravivamsi.github.io/postmanframework/

