const fs = require('fs');
const tools = require('../network/toolbox.js'); 

function prepareData(data,collectionName){
    //const iteracion = profile.iterationCount;
    const iterations_executed = data.iterations.total-data.iterations.pending;
    const iterations_failed = data.iterations.failed;
    const requests_executed=data.requests.total-data.requests.pending;
    const requests_failed =data.requests.failed;
    const testScripts_executed=data.testScripts.total-data.testScripts.pending;
    const testScripts_failed=data.testScripts.failed;
    const prerequestScripts_executed= data.prerequestScripts.total-data.prerequestScripts.pending;
    const prerequestScripts_failed=data.prerequestScripts.failed;
    const assertions_executed = data.assertions.total-data.assertions.pending;
    const assertions_failed =data.assertions.failed;

    data = {
        "profile":{
            "name":collectionName || 'NA',
            "iteracion":"NA"
        },
        "iterations":{
           "executed":iterations_executed,
           "failed":iterations_failed
        },
        "requests":{
           "executed":requests_executed,
           "failed":requests_failed
        },
        "testScripts":{
           "executed":testScripts_executed,
           "failed":testScripts_failed
        },
        "prerequestScripts":{
            "executed":prerequestScripts_executed,
            "failed":prerequestScripts_failed
        },
        "assertions":{ 
            "executed":assertions_executed,
            "failed":assertions_failed
        }
     }
    return data;
}


async function  wirteHtmlUl(dir){
  
    var fileList =  "";
    const initulMenu =  ' <ul id="menu"> ';
    var   ilMenu= '';
    const fintulMenu =' </ul> ';
    var htmlMenu ='';

    const menu = await tools.readFilesFolder(dir).then((files => {
        fileList = files;
        if (fileList.length > 0){
            for (i =0; i < fileList.length; i++){
                if (fileList[i] !=='index.html'){
                    ilMenu +=  ` <a href="/app/${fileList[i]}"><li>${fileList[i]}</li></a> `;
                }
            }
            htmlMenu = initulMenu+ilMenu+fintulMenu;
        }else{
            htmlMenu = initulMenu + fintulMenu;
        }
        return htmlMenu;
    })).catch(error =>{
        console.log(error);
      })
      return menu;
}

async function wirteHtmlReportFile(dir) {
    const ulMenu = await wirteHtmlUl(dir);
    const header =  tools.readFile('./htmlreport/header.txt');
    const footer =  tools.readFile('./htmlreport/footer.txt');
    var allHtml= header+ulMenu+footer;
    allHtml = await tools.writeFile(allHtml,dir+'/index.html');
};



module.exports = {
    prepareData,
    wirteHtmlReportFile
}
