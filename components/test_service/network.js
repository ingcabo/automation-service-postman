const express = require('express');
const response = require('../../network/response');
const router = express.Router();
const pkg = require('../../package.json');


router.get('/', (req, res) => {
  console.log("v " + pkg.version);
  response.success(req,res,` ${pkg.name}  v ${pkg.version}`,200);
});


module.exports = router;