const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const cfg =require('../../cfg.json');
const jsonFileTestsStorage = `${cfg.dirTesfile}/${cfg.fileTestName}`;
const router = express.Router();

router.post('/', (req, res) => {  
   
    let obj="";
    if (!req.body){
        response.error(req, res, "there is no json in the message", 500, "there is no json in the message");
    } 
    obj =req.body;

    try{
        //JSON.parse(obj);
        controller.createFile(obj,jsonFileTestsStorage).then((result=>{
            response.success(req,res,"We created the Test Set!!!!",200);    
        })).catch((err)=>{
            response.error(req, res, err, 500, err);
        })
    }catch (err){
        response.error(req, res, err, 500, err);
    }
});


router.get('/', (req, res) => {
    let result = "";
    controller.getFileTest(jsonFileTestsStorage).then((resp)=>{
        if (req.query.testname) {
            result = resp[req.query.testname];
        }else{result = resp}
        response.success(req,res,result,200);
    }).catch((err=>{
        response.error(req,res,err,500,err);
    }))
  });

  router.delete('/:id', (req, res) => {
    if (req.params.id){
        controller.deleteFromObjet(req.params.id,jsonFileTestsStorage).then((result=>{
            response.success(req,res,"Set Removed!!",200);    
         })).catch((err)=>{
            response.error(req, res, err, 500, err);
         })
    }else{
        response.error(req, res, "missing id parameter", 401, "missing id parameter");
    }  
  });

  module.exports = router;