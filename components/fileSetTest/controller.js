const tools = require('../../network/toolbox'); 

function getFileTest(file) {
    return new Promise((resolve, reject) => {
        let data = JSON.parse(tools.readFile(file));
        resolve(data);
    })
}

//creating a new test
function createFile(obj, file) {
    return new Promise((resolve, reject) => {
        let data = tools.readFile(file);
        if (data.length > 0) {
            data = JSON.parse(data);
            data = { ...data, ...obj };
        } else {
            data = obj;
        }
        tools.writeFile(data,file);
        resolve("set created");
    });
}

//deleting a test
function deleteFromObjet(tesName, file) {
    return new Promise((resolve, reject) => {
        data = JSON.parse(tools.readFile(file));
        delete data[tesName];
        tools.writeFile(data,file);
        resolve("set removed");
    });
}

module.exports = {
    createFile,
    getFileTest,
    deleteFromObjet
}