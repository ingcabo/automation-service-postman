const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const router = express.Router();

router.post('/',function(req,res){
    controller.addUser(req.body.name)
    .then((data) =>{
        response.success(req,res,data,200);
    })
    .catch(err =>{
        response.error(req,res,'internal error user add post', 500,err);
    });
});


router.get('/', function(req,resp){
    controller.listUser().then((users)=>{
        response.ssuccess(req,res,users,200);
    }).catch((err) =>{
        response.error(req,resp,'internak error get',500,err)
    })
})

module.exports = router;