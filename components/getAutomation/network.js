const express = require('express');
const response = require('../../network/response');
const logger = require('../../network/logger');
const router = express.Router();
const tools = require('../../network/toolbox.js');
const summaryReport = require('../../htmlreport/summaryReport.js')
const controller = require('../../controllers/controller');
const fs = require('fs');
const { dirTesfile, fileTestName, dirReport } = require('../../cfg.json');
const jsonFileTestsStorage = `${dirTesfile}/${fileTestName}`;




router.get('/', async function (req, res) {
    var testName = "";
    if (!req.query.test) {

        var msg = "specify ?test=testname parameters";
        // response.msgSlack(msg);
        sendMssge(msg);
        response.error(req, res, msg, 500, msg);

    } else {

        const data = tools.readFile(jsonFileTestsStorage) //fs.readFileSync(jsonFileTestsStorage);
        var profile = JSON.parse(data);
        testName = req.query.test;

        if (!profile[testName]) {
            let message = "=testname name doesn't exist!!!"
            sendMssge(message);
            response.error(req, res, message, 500, msg);

        } else {

            profile = profile[testName];

            for (var i = 0; i < profile.length; i++) {
                let timeForResportTestNameHtml = tools.formatDate();
                let testParams = { ...profile[i], timeForResportTestNameHtml }

                await controller.runcollection(testParams).then((result) => {
                    logger.info(JSON.stringify(result));
                    let collectionName = result.collection.name;
                    var data = {
                        summary: summaryReport.prepareData(result.run.stats, collectionName),
                        failures: result.run.failures,
                        timings: result.run.timings
                    };

                    //we create the html to see the reports
                    summaryReport.wirteHtmlReportFile(dirReport);
                    //we are sending de report summary by Slack Web hooks
                    sendMssge(data, testParams);
                    response.success(req, res, data, 200);

                }).catch(err => {
                    logger.error(err.message)
                    logger.error(err.stack)
                    response.error(req, res, err.message, 500, err.stack);
                })
            }


        }

    }

});


router.post('/', async function (req, res) {
    var testName = "";

    if (!req.body.text) {

        var msg = "specify ?test=testname parameters";
        //enviamos un texto a slack
        // response.msgSlack(msg);
        sendMssge(msg);
        response.error(req, res, msg, 500, msg);

    } else {

        const data = fs.readFileSync('./testfile/profile.json');
        var profile = JSON.parse(data);
        //console.log(profile);
        testName = req.body.text.trim();

        if (!profile[testName]) {

            var msg = "=testname name doesn't exist!!!";
            // response.msgSlack(msg);
            sendMssge(msg);
            response.error(req, res, msg, 500, msg);

        } else {

            profile = profile[testName];
            for (var i = 0; i < profile.length; i++) {
                profiled = profile[i];
                await controller.runcollection(profiled).then(result => {

                    var data = {
                        summary: tools.prepareData(result.stats, profiled),
                        failures: result.failures,
                        timings: result.timings
                    };

                    sendMssge(data);
                    response.success(req, res, data, 200);
                }).catch(err => {
                    response.error(req, res, err, 500, err);
                })

            }

        }

    }

});

function sendMssge(msg, testParams = "") {
    const WEBHOOK_TYPE = process.env.WEBHOOK_TYPE;

    if (WEBHOOK_TYPE == 2) {
        response.msgTeams(msg, testParams);
    } else if (WEBHOOK_TYPE == 1) {
        response.msgSlack(msg, testParams);
    } else {
        response.msgTeams(msg, testParams);
        response.msgSlack(msg, testParams);
    }

}

module.exports = router;


