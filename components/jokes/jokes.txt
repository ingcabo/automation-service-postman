When I see lovers' names carved in a tree, I don't think it's sweet. I just think it's surprising how many people bring a knife on a date.
That bizarre moment when you pick up your car from the garage and you realize that the breaks are still not working, but they made your horn louder.
I had a dream where an evil queen forced me to eat a gigantic marshmallow. When I woke up, my pillow was gone.
Dentist: "You need a crown."  - Patient: "Finally someone who understands me."
A guest is ordering at a restaurant, “Do you think you could bring me what that gentleman over there is having? - The waiter looks at him sternly, “No sir, I’m very sure he intends to eat it himself.”
“Waiter, the steak smells very strongly of liquor!” - The waiter backs up 3 steps and asks, “How’s that now?”