const express = require('express');
const response = require('../../network/response');
const {generateJsonFileController,validationController} = require('./controller');
const tools = require('../../network/toolbox');
const { dirBatchfile } = require('../../cfg.json')
const router = express.Router();

router.post('/upload', async (req, res) => {
  try {
    if (!req.files) {
      response.error(req, res, "you must select files to upload data,env,collection,filetest", 500, "you must select files to upload");
    } else {

      if (req.files.batch_file) {
        let file = req.files.batch_file;
        var fileData = await tools.moveFile(file, dirBatchfile);
        var fileData = await generateJsonFileController(file.name);
        response.success(req, res, {"file":dirBatchfile+file.name}, 200);
      }

    }
  } catch (err) {
    response.error(req, res, err, 500, err);
  }
});

router.post('/validation', async (req, res) => {
  try {
    if (!req.body) {
      response.error(req, res, "xxxxxxx", 500, "xxxxxxx");
    } else {

              await validationController(req.body).then((result) => {
  
                console.log("result",result)
                
                response.success(req, res,result, 200);

            }).catch(err => {

                response.error(req, res, err.message, 500, "error");
            })

    }
  } catch (err) {
    response.error(req, res, err, 500, err);
  }
});




module.exports = router;

