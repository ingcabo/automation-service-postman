const tools = require('../../network/toolbox');
const fs = require('fs-extra')
const { dirBatchfile } = require('../../cfg.json')
const { lengthValidationObjectJson, lengthValidation, validateFormat } = require('./generalRules.js');


async function applyRules(object) {
    const { file_name, count_column, details } = object;
    let packageObj = await JSON.parse(tools.readFile(dirBatchfile + file_name + ".json"));
    let validationArray = [];

    // validation 1
    let sizeJsonValidationArray = validateJsonSize(packageObj, count_column);
    // validation 1

    details.forEach(element => {

        for (var i = 0; i < packageObj.length; i++) {

            let count = Object.keys(packageObj[i]).length - 2;

            for (var j = 0; j <= count + 1; j++) {
                let JsonFileFieldName = Object.keys(packageObj[i])[j];

                if (JsonFileFieldName == element.name) {
                    let validation_2_lengthValidation = lengthValidation(packageObj[i][`${JsonFileFieldName}`], element.min_length, element.max_length);

                    if (validation_2_lengthValidation.length > 0) {
                        let object = buildObject(validation_2_lengthValidation, element.format, JsonFileFieldName, packageObj[i], i + 2);
                        validationArray.push(object);
                    }



                    let validation_3_formatValidation = validateFormat(packageObj[i][`${JsonFileFieldName}`], element.format,element.min_length);
                    if (validation_3_formatValidation.length > 0) {
                        let object = buildObject(validation_3_formatValidation, element.format, JsonFileFieldName, packageObj[i], i + 2);
                        validationArray.push(object);
                    }



                }

            }
        }

    });

    let structure = {
        columns: sizeJsonValidationArray,
        validation:validationArray
    }
    return structure;
}

function validateJsonSize(packageObj, count_column) {
    var arr = []
    let objct = {};
    for (var i = 0; i < packageObj.length; i++) {
        let validation_1_numberOfColumns = lengthValidationObjectJson(packageObj[i], count_column);

        if (validation_1_numberOfColumns.length > 0) {
            object = {
                line: i + 2,
                description: validation_1_numberOfColumns,
                object: packageObj[i]
            }
            arr.push(object);

        }
    }
    return arr;
}

function buildObject(description, format, fieldName, object, line) {

    object = {
        field_name: fieldName,
        line,
        description,
        format,
        object
    }

    return object;
}



module.exports = {
    applyRules
}

