const NORMATIVOS = "NORMATIVOS";
const APER_OBLIGA= "APER_OBLIGA";
const OBLIGA_VIGENT= "OBLIGA_VIGENT";
const RENOVACION= "RENOVACION";


module.exports = {
    NORMATIVOS,
    APER_OBLIGA,
    OBLIGA_VIGENT,
    RENOVACION
}

/*
var regExpPassword = /^[ a-zA-Z]*/;
var password1 = 'J@v@scr1pt'
var password2 = 'N0d3js'
var password3 = 'umbr3ll@'
var password4 = 'A1rpl@ne'

/*
console.log(`Test ${password1}:`+regExpPassword.test(password1));
console.log(`Test ${password2}:`+regExpPassword.test(password2));
console.log(`Test ${password3}:`+regExpPassword.test(password3));
console.log(`Test ${password4}:`+regExpPassword.test(password4));
*/