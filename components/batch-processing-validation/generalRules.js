
function lengthValidation(value, minLength,maxnLength){
    
    let errorMessage = "";
    if (minLength == 0){
        errorMessage = "";
    } else if(value.length < minLength || value.length > maxnLength){
        errorMessage = "field should be " +minLength+ " to " +maxnLength+ " characters.";
    }else{
        errorMessage = "";
    }
    return errorMessage;
 }

 function lengthValidationObjectJson(object, countColumn){

    let errorMessage = "";
    let count = Object.keys(object).length;

    if (count != countColumn){
        errorMessage = "the number of columns sent is not as expected";
    }

    return errorMessage;
 }

 function validateFormat(value, format,min_length) {
  
   let regExpValidation ="";
    if (format == 'YYMMDD'){
        regExpValidation = validateDateFormat(value);
    }else if(format == 'DD/MM/YYYY'){
        regExpValidation = validateDateFormatV2(value);
    }else if(format == 'string'){
        regExpValidation =validateStringFormat(value,min_length);
    }else if(format == 'numeric'){
        regExpValidation = validateNumericFormat(value,min_length);
    }else if(format == 'alphanumeric'){
        regExpValidation = validateAlphaNumericFormat(value,min_length);
    }else if(format == 'double'){
        regExpValidation =validateAmount(value,min_length);
    }else if(format == 'amount'){
            regExpValidation =validateAmountv2(value,min_length);
    }else{
        //YYMMDD
        regExpValidation = validateAmount(value,min_length);
    }

    return regExpValidation;
};

function validateDateFormat(dateString){

    let regExpPassword =  /([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))/;
    let errorMessage="";

    if (!regExpPassword.test(dateString)){
        errorMessage =  errorMessage = "error validating date field";
    }

    return errorMessage;
}

function validateDateFormatV2(dateString){

    let regExpPassword =  /^(?=\d{2}([-.,\/])\d{2}\1\d{4}$)(?:0[1-9]|1\d|[2][0-8]|29(?!.02.(?!(?!(?:[02468][1-35-79]|[13579][0-13-57-9])00)\d{2}(?:[02468][048]|[13579][26])))|30(?!.02)|31(?=.(?:0[13578]|10|12))).(?:0[1-9]|1[012]).\d{4}$/;
    let errorMessage="";

    if (!regExpPassword.test(dateString)){
        errorMessage =  errorMessage = "error validating date field";
    }

    return errorMessage;
}

function validateAmount(amount,min_length){

    let regExpPassword = /^[0-9]{1,3}(\.[0-9]{3})*\,[0-9]+$/;
    let errorMessage="";

    if (!regExpPassword.test(amount)){
        errorMessage = "error validating amount field  format 1.000,00";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}

function validateAmountv2(amount,min_length){

    let regExpPassword = /^\d{0,12}(\.\d{1,2})?$/;
    let errorMessage="";

    if (!regExpPassword.test(amount)){
        errorMessage = "error validating amount field format 10.00";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}


function validateAlphaNumericFormat(value,min_length){
    let errorMessage="";
    let regExpPassword = /^[a-z\d\-_\s]+$/;

    if (!regExpPassword.test(value)){
        errorMessage = "it is not alphanumeric";
    }

    return errorMessage;
}

function validateNumericFormat(value,min_length){
    let errorMessage="";
    let regExpPassword = /^[0-9]+$/;

    if (!regExpPassword.test(value)){
        errorMessage = "it is not numeric";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}



function validateStringFormat(value,min_length){
    let errorMessage="";
    let regExpPassword = /^[a-zA-Z\d\-ñÑ�\s]+$/;

    if (!regExpPassword.test(value)){
        errorMessage = "it is not alphabetical format";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}

function validateNumericFormat(value,min_length){
    let errorMessage="";
    let regExpPassword = /^-?\d+$/;

    if (!regExpPassword.test(value)){
        errorMessage = "it is not numeric";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}


module.exports = {
    validateFormat,
    lengthValidationObjectJson,
    lengthValidation,
    validateAmount,
    validateNumericFormat,
    validateAlphaNumericFormat

}
